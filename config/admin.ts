export default ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', 'cb85dec39c8349be823def307e6358ae'),
  },
});
